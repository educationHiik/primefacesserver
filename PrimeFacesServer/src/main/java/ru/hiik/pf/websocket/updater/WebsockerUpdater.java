/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.pf.websocket.updater;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author vaganovdv
 */
@Named
@SessionScoped
public class WebsockerUpdater implements  Serializable 
{
 
    @Inject @Push (channel="gui")
    private PushContext push;
    private int count = 0;
    
    private String text;
    
    
    public void add()
    {
       count ++;
       this.text = ""+ count;
       push.send("update");
    }        
    
     /**
     * @return the text
     */
    public String getText()
    {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text)
    {
        this.text = text;
    }
}
